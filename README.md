# Mukuru DevOps Assessment

The application consists of a backend only based on go-lang.
* There is a docker file and a docker compose file that can be used to run the application  
* https://gitlab.com/moleisking/docker-compose-demo

## Build 

To build the "frontend" docker image
> `docker image build -t docker-compose-demo .`

## Run in Windows terminal

To start the frontend which can be found in the "docker-compose-demo" folder
> `go run ./src/mukuru-http.go`

To run the frontend using docker which can be found in the "docker-compose-demo" folder
> `docker container run -p 8080:3000 docker-compose-demo -e "VERSION=2.0" -e "DOCKER_COMPOSE_DEMO_SERVER_PROTOCOL=http"`

To run the frontend using docker-compose which can be found in the "docker-compose-demo" folder
> `docker compose up`

## Set environmental variable
### Windows PowerShell
#### Write
> `$Env:DOCKER_COMPOSE_DEMO_SERVER_HOST="127.0.0.1"`
> `$Env:DOCKER_COMPOSE_DEMO_SERVER_PORT="3000"`
> `$Env:DOCKER_COMPOSE_DEMO_SERVER_PROTOCOL="http"`
> `$Env:VERSION="1.0"`
#### Read
> `echo $Env:DOCKER_COMPOSE_DEMO_SERVER_HOST $Env:DOCKER_COMPOSE_DEMO_SERVER_PORT $Env:DOCKER_COMPOSE_DEMO_SERVER_PROTOCOL $Env:VERSION`
#### List
> `Get-ChildItem Env:`

### Linux Terminal
#### Write
> `export DOCKER_COMPOSE_DEMO_SERVER_HOST="127.0.0.1"`
> `export DOCKER_COMPOSE_DEMO_SERVER_PORT="3000"`
> `export DOCKER_COMPOSE_DEMO_SERVER_PROTOCOL="http"`
> `export VERSION="1.0"`
#### Read
> `echo $DOCKER_COMPOSE_DEMO_SERVER_HOST $DOCKER_COMPOSE_DEMO_SERVER_PORT $DOCKER_COMPOSE_DEMO_SERVER_PROTOCOL $VERSION`
## Links
* [When running the application on go and terminal](http://127.0.0.1:3000)
* [When running the application on docker](http://127.0.0.1:8080)

## Intent
* Display familiarity with Docker, GIT & Pipelines
* Ability to study new technologies and reach an outcome within a timeframe
* Communicate the solution through configuration files & repository README

## Overview
_The goal of this exercise is to dockerise this application, to prepare it for a deployment into kubernetes, and to make the build and deployment consistent. Some of these steps are optional, so you can choose what you'd like to focus on, depending on the time you have available._

## Task
### Dockerise the application
* Create a free GitHub / GitLab / BitBucket account using any name (or use your existing personal account if you like) - https://gitlab.com,  https://github.com  or https://bitbucket.org/ & create a public repository called docker-compose-demo (GitLab is recommended as it provides pipelines and a container registry)
* Add a Docker file to allow a Docker image to be created for the application
* Add a docker-compose file to allow the application to run simply by using the docker-compose up command, and navigating to http://localhost:8080/. This should allow anyone to clone the repo and run the application with only docker installed (no need for Go)

### Automate the above build process with a pipeline (optional)
* Setup a build / publish pipeline to build the images on every commit
* As a part of the pipeline, push the built docker image to a docker registry automatically (as mentioned above GitLab provides pipelines & a docker image registry, otherwise you can create a free Docker Hub (https://hub.docker.com/) account)

### Configure a kubernetes deployment (optional)
* Using something like Docker for Mac / Windows local kubernetes cluster, configure a method to deploy this application into kubernetes
* This application should be available from localhost without needing to proxy into the cluster
* Scale the instance to 4 replicas

### Enable HTTPS (optional) 
* Allow the application to be served over HTTPS